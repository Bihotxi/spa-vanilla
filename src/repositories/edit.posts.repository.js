import axios from "axios";

export class EditPostRepository {
  async updatePost(post) {
    const postDto = {
      id: post.id,
      title: post.title,
      body: post.content,
      userId: 1,
    };

    return await (
      await axios.put(
        `https://jsonplaceholder.typicode.com/posts/${post.id}`,
        postDto
      )
    ).data;
  }
}
