import axios from "axios";

export class DeletePostRepository {
  async deletePost(id) {
    try {
      const response = await axios.delete(
        `https://jsonplaceholder.typicode.com/posts/${id}`
      );

      return response.data;
    } catch (error) {
      console.error("Error al eliminar post:", error);
      throw error;
    }
  }
}
