import axios from "axios";

export class CreatePostRepository {
  async createPost(newPost) {
    return await (
      await axios.post("https://jsonplaceholder.typicode.com/posts", newPost)
    ).data;
  }
}
