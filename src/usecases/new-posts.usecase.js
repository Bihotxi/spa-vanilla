import { CreatePostRepository } from "../repositories/create.posts.repository";
import { Post } from "../model/post";

export class CreatePostUseCase {
  async execute(title, content) {
    const repository = new CreatePostRepository();
    const newPost = {
      title: title,
      body: content,
    };
    const response = await repository.createPost(newPost);
    return new Post({
      id: response.id,
      title: response.title,
      content: response.body,
    });
  }
}
