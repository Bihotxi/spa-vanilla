import { DeletePostRepository } from "../repositories/delete.posts.repository";

export class DeletePostsUseCase {
  static async execute(id) {
    try {
      const repository = new DeletePostRepository();
      const deletedPost = await repository.deletePost(id);

      if (deletedPost) {
        return { status: 200, statusText: "OK" };
      } else {
        return { status: 400 };
      }
    } catch (error) {
      console.error("Error al eliminar el post:", error);
      throw error;
    }
  }
}
