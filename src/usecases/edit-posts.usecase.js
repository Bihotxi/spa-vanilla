import { EditPostRepository } from "../repositories/edit.posts.repository";

export class EditPostUseCase {
  static async execute(posts = [], postModel) {
    const repository = new EditPostRepository();
    const postUpdated = await repository.updatePost(postModel);
    const postModelUpdated = {
      id: postUpdated.id,
      title: postUpdated.title,
      content: postUpdated.body,
    };

    return posts.map((post) =>
      post.id === postModelUpdated.id
        ? (post = postModelUpdated)
        : (post = post)
    );
  }
}
