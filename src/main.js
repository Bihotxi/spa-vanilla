import { Router } from "@vaadin/router";
import "./main.css";
import "./pages/posts.page";

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "posts-page" },
  { path: "(.*)", redirect: "/" },
]);
