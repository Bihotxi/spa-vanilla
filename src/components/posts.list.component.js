import { LitElement, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import "../components/posts.content.component";
import "../components/posts.item.component";

export class PostsListComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    this.posts = await AllPostsUseCase.execute();

    document.addEventListener("post:update", (e) => {
      const updatedPost = e.detail.updatedPost;
      const updatedPosts = this.posts.map((post) =>
        post.id === updatedPost.id ? updatedPost : post
      );
      this.posts = updatedPosts;
    });

    document.addEventListener("post:delete", (e) => {
      const deletedPostId = e.detail.deletedPostId;
      const updatedPosts = this.posts.filter(
        (post) => post.id !== deletedPostId
      );
      this.posts = updatedPosts;
    });
  }

  render() {
    return html`      
        <section class="posts-list__container">
          <div class="posts-list__button-container">
            <button id="add-button" @click=${
              this.handleToHiddeButtons
            }>Add</button>
          </div>
          <h3 id="title-post__section" class="posts-list__title">Posts List</h3>
          <ul class="posts-list__list">
            ${this.posts?.map(
              (post) => html`<posts-item-component .post="${post}" />`
            )}
          </ul>
        </section>
      </section>
    `;
  }

  handleToHiddeButtons() {
    document.querySelector("#update-button").classList = "hidden";
    document.querySelector("#delete-button").classList = "hidden";
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("posts-list-component", PostsListComponent);
