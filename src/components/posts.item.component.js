import { LitElement, html, css } from "lit";
import "../ui/title.posts.ui";

export class PostsItemComponent extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html`
      <li
        id="post_${this.post?.id}"
        @click="${() => this.handleTitlePostClick(this.post)}">
        <title-posts-ui .post="${this.post}"></title-posts-ui>
      </li>
    `;
  }

  handleTitlePostClick(post) {
    const postToInsert = new CustomEvent("post:show", {
      bubbles: true,
      detail: { post },
    });
    this.dispatchEvent(postToInsert);
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("posts-item-component", PostsItemComponent);
