import { LitElement, html } from "lit";
import { EditPostUseCase } from "../usecases/edit-posts.usecase";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { DeletePostsUseCase } from "../usecases/delete-posts.usecase";

export class PostsContentComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      selectedPostId: { type: Number },
      selectedPostTitle: { type: String },
      selectedPostContent: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
    this.selectedPostId = null;
    this.selectedPostTitle = "";
    this.selectedPostContent = "";
    this.contentComponentUpdateButton = this.querySelector("#update-button");
    this.contentComponentDeleteButton = this.querySelector("#delete-button");

    document.addEventListener("post:show", (e) => {
      const post = e.detail.post;
      this.selectedPostId = post.id;
      this.selectedPostTitle = post.title;
      this.selectedPostContent = post.content;
      this.contentComponentUpdateButton.classList = "visible";
      this.contentComponentDeleteButton.classList = "visible";
    });
  }

  render() {
    return html`
      <section class="content-section ">
        <h3>Post Detail</h3>
        <form class="posts-form">
        <div class="form-row" >
          <label for="posts-title-textarea" .value="${this.selectedPostTitle}"
            >Title</label
          >
          <input
            id="posts-title-textarea"
            type="text"
            .value="${this.selectedPostTitle ?? ""}"
            @input="${this.handleTitleInput}" />
        </div>
        <div class="form-row">

          <label for="posts-content-textarea" .value="${
            this.selectedPostContent
          }"
            >Body</label
          >
          <input
            name="body-textarea"
            id="posts-content-textarea"
            type="text"
            .value="${this.selectedPostContent ?? ""}"
            @input="${this.handleContentInput}"></textarea>
       </div>
       <div class="form-row form-row__button">
          <button
            @click="${this.handleCancelButton}"
            id="cancel-button"
            type="reset">
            Cancel
          </button>
          <button
            @click=${this.handleUpdatePost}
            id="update-button"
            class="hidden"
            type="submit">
            Update
          </button>
          <button
            @click=${this.handleDeletePost}
            id="delete-button"
            class="hidden"
            type="submit">
            Delete
          </button>
      </div>
        </form>
      </section>
    `;
  }

  createRenderRoot() {
    return this;
  }

  handleCancelButton() {
    document.querySelector("#update-button").classList = "hidden";
    document.querySelector("#delete-button").classList = "hidden";
  }

  handleTitleInput(event) {
    this.selectedPostTitle = event.target.value;
  }

  handleContentInput(event) {
    this.selectedPostContent = event.target.value;
  }

  async handleUpdatePost(event) {
    event.preventDefault();
    const title = this.querySelector("#posts-title-textarea").value;
    const content = this.querySelector("#posts-content-textarea").value;
    const postModel = {
      id: this.selectedPostId,
      title,
      content,
    };

    const updatedPosts = await EditPostUseCase.execute(this.posts, postModel);
    this.posts = updatedPosts;

    const postUpdated = new CustomEvent("post:update", {
      bubbles: true,
      detail: {
        updatedPost: postModel,
      },
    });

    this.dispatchEvent(postUpdated);
  }

  async handleDeletePost(event) {
    event.preventDefault();
    const deletedPostId = this.selectedPostId;

    this.selectedPostTitle = "";
    this.selectedPostContent = "";

    try {
      const deletedResult = await DeletePostsUseCase.execute(deletedPostId);

      if (deletedResult.status === 200) {
        console.log("Post eliminado");
        const postDeleted = new CustomEvent("post:delete", {
          bubbles: true,
          detail: {
            deletedPostId: deletedPostId,
          },
        });
        this.dispatchEvent(postDeleted);
      }
    } catch (error) {
      console.error("Error al eliminar el post", error);
    }
  }
}

customElements.define("posts-content-component", PostsContentComponent);
