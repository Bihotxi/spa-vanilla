import "../components/posts.list.component";
import "../components/posts.content.component";

export class PostsPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
    <header class="posts__header">
       <h1 class="logo">LOGO</h1>
    </header>
    <div class="posts__container">
      <div class="posts__heading"><h2 class="posts-components__title">POSTS</h2></div>
      <section class="posts__sections">        
          <posts-list-component></posts-list-component>
          <posts-content-component class="content-section__container"></posts-content-component>
      </section>
    </div>
    `;
  }
}

customElements.define("posts-page", PostsPage);
