import { LitElement, html, css } from "lit";

export class TitlePostsUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html` <span id="title">${this.post?.title} </span> `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("title-posts-ui", TitlePostsUI);
