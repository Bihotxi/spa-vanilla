describe("Spa-vanilla home page", () => {
  it("should show the home page", () => {
    cy.visit("localhost:8080/posts");
    cy.get("#title-post__section").should("be.visible");
    cy.get(".content-section > h3").should("be.visible");
  });
  it("should display in the form the selected post and buttons", () => {
    cy.visit("localhost:8080/posts");
    let titleText;

    cy.get("#posts-content-textarea").should("have.value", "");

    cy.get("#post_1 > title-posts-ui > #title")
      .click()
      .then((title) => {
        titleText = title.text().trim();
      });

    cy.get("#posts-title-textarea")
      .invoke("val")
      .should((titleTextareText) => {
        expect(titleTextareText).to.equal(titleText);
      });

    cy.get("#posts-content-textarea").should("not.have.value", "");

    cy.get("#update-button").should("have.class", "visible");
    cy.get("#delete-button").should("have.class", "visible");
  });
  it("should hidde in the form the update and the delete buttons, and clean inputs fields", () => {
    cy.visit("localhost:8080/posts");
    cy.get("#cancel-button").click();

    cy.get("#posts-content-textarea").should("have.value", "");
    cy.get("#posts-title-textarea").should("have.value", "");

    cy.get("#update-button").should("have.class", "hidden");
    cy.get("#delete-button").should("have.class", "hidden");
  });
  it("should hidde in the form the update and the delete buttons and the inputs text visible", () => {
    cy.visit("localhost:8080/posts");
    let titleText;

    cy.get("#posts-content-textarea").should("have.value", "");

    cy.get("#post_1 > title-posts-ui > #title")
      .click()
      .then((title) => {
        titleText = title.text().trim();
      });

    cy.get("#posts-title-textarea")
      .invoke("val")
      .should((titleTextareText) => {
        expect(titleTextareText).to.equal(titleText);
      });

    cy.get("#posts-content-textarea").should("not.have.value", "");

    cy.get("#update-button").should("have.class", "visible");
    cy.get("#delete-button").should("have.class", "visible");

    cy.get("#add-button").click();

    cy.get("#update-button").should("have.class", "hidden");
    cy.get("#delete-button").should("have.class", "hidden");
  });
});
