import { POSTS } from "./fixtures/posts";
import { PostsRepository } from "../src/repositories/posts.repository";
import { DeletePostsUseCase } from "../src/usecases/delete-posts.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Delete a post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should delete a post", async () => {
    const postIdToDelete = 35;
    const postToDelete = POSTS.find((post) => post.id === postIdToDelete);
    if (!postToDelete) {
      throw new Error(`Post with id ${postIdToDelete} not found`);
    }

    const deleteResult = await DeletePostsUseCase.execute(postIdToDelete);

    expect(deleteResult.status).toBe(200);
    expect(deleteResult.statusText).toBe("OK");
  });
});
