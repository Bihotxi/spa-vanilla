import { PostsRepository } from "../src/repositories/posts.repository";
import { CreatePostUseCase } from "../src/usecases/new-posts.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Create post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should create a new post", async () => {
    const title = "New Post Title";
    const content = "New post content";

    const createPostUseCase = new CreatePostUseCase();

    const result = await createPostUseCase.execute(title, content);

    expect(result.title).toBe(title);
    expect(result.content).toBe(content);
  });
});
