import { PostsRepository } from "../src/repositories/posts.repository";
import { EvenPostsUseCase } from "../src/usecases/even-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("All even posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all even posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await EvenPostsUseCase.execute();

    expect(posts.every((post) => post.id % 2 === 0)).toBe(true);
  });
});
